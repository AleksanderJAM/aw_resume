
import UIKit

protocol DetailsCell: UITableViewCell {
    func configure(using model: Decodable)
}
