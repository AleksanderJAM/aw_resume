
import Foundation

protocol BaseViewModel: AnyObject {
    var items: [T] { get }
    func getData(success: @escaping () -> Void,
                 failure: @escaping (Error) -> Void)
    associatedtype T: Decodable
    
    init()
}
