
import UIKit

protocol ViewBuilder {
    func setupProperties()
    func setupHierarchy()
    func setupAutoLayout()
}

extension ViewBuilder {
    
    func setupViews() {
        setupProperties()
        setupHierarchy()
        setupAutoLayout()
    }
    
    func buildView() -> UIView {
        return UIView()
    }
    
    func buildStackView(axis: NSLayoutConstraint.Axis = .vertical,
                        alignment: UIStackView.Alignment = .center,
                        spacing: CGFloat = 26) -> UIStackView {
        let stackView = UIStackView().manualLayoutable()
        stackView.axis = axis
        stackView.alignment = alignment
        stackView.spacing = spacing
        stackView.isUserInteractionEnabled = true
        return stackView
    }
    
    func buildMainStackView() -> UIStackView {
        let stackView = self.buildStackView(axis: .vertical, alignment: .fill, spacing: 20)
        return stackView
    }
    
    func buildLabel(type: UILabel.LabelType = .value, text: String = "", numberOfLines: Int = 1) -> UILabel {
        let label = UILabel()
        let isLabel = type == .label
        let boldFont = UIFont.appFont(ofSize: 15, weight: .bold)
        let regularFont = UIFont.appFont(ofSize: 15, weight: .regular)
        label.font = isLabel ? boldFont : regularFont
        label.text = text
        label.textAlignment = isLabel ? .left : .right
        label.numberOfLines = numberOfLines
        return label
    }
}
