import Foundation

enum APIServiceError: LocalizedError {
    case responseError(String)
}
