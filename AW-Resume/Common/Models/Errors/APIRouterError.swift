
import Foundation

enum APIRouterError: LocalizedError {
    case cannotCreateValidURL
    
    var errorDescription: String? {
        switch self {
        case .cannotCreateValidURL:
            return "Unable to connect to the server. Invalid request URL."
        }
    }
}
