
import Foundation

class Education: Decodable {
    let schoolName: String
    let degreeName: String
    let graduationYear: String
}
