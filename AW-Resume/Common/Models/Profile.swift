
import Foundation

struct Profile: Decodable {
    let firstName: String
    let lastName: String
    var photoUrl: URL? {
        return URL(string: photoPath)
    }
    private let photoPath: String
    
    enum CodingKeys: String, CodingKey {
        case firstName
        case lastName
        case photoPath = "photoUrl"
    }
}
