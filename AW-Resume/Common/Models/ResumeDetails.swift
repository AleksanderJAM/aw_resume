
import Foundation

enum ResumeDetails {
    case education
    case experience
    case projects
    
    var title: String {
        switch self {
        case .education: return "_education"
        case .experience: return "_experience"
        case .projects: return "_projects"
        }
    }
}
