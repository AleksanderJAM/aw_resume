
import Foundation

class Project: Decodable {
    let name: String
    let description: String
    let role: String
    let clientOrigin: String
    let usedTechnologies: [String]
    private let appStorePath: String?
    
    var appStoreUrl: URL? {
        return URL(string: appStorePath ?? "")
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case role
        case clientOrigin
        case usedTechnologies
        case appStorePath = "appStoreUrl"
    }
}
