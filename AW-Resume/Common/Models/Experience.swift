
import Foundation

class Experience: Decodable {
    
    // MARK: - Properties
    
    let companyName: String
    let position: String
    private let companyLogoPath: String
    private let companyWebsitePath: String
    private let startDateString: String
    private let endDateString: String?
    
    // MARK: - Convinience getters
    
    var startDate: Date? {
        return ISO8601DateFormatter().date(from: self.startDateString)
    }
    
    var endDate: Date? {
        return ISO8601DateFormatter().date(from: self.endDateString ?? "")
    }
    
    var companyLogoUrl: URL? {
        return URL(string: self.companyLogoPath)
    }
    
    var companyWebsiteUrl: URL? {
        return URL(string: self.companyWebsitePath)
    }
    
    enum CodingKeys: String, CodingKey {
        case companyName
        case position
        case companyLogoPath = "companyLogoUrl"
        case companyWebsitePath = "companyWebsiteUrl"
        case startDateString = "startDate"
        case endDateString = "endDate"
    }
}
