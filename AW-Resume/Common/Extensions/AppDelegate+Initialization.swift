
import UIKit

extension AppDelegate {
    
    func performInitialSetup() {
        setupWindow()
        setupDesign()
        setupInitialScene()
    }
    
    private func setupWindow() {
        self.window = UIWindow()
    }
    
    private func setupDesign() {
        self.window?.tintColor = .black
        self.window?.backgroundColor = .appPrimary
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().backgroundColor = .appPrimary
    }
    
    private func setupInitialScene() {
        self.window?.rootViewController = setupMainAppFlowScene()
        self.window?.makeKeyAndVisible()
    }
    
    private func setupMainAppFlowScene() -> UIViewController {
        return AWNavigationController(rootViewController: HomeViewController())
    }
    
}
