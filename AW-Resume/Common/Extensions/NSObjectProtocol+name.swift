
import Foundation

extension NSObjectProtocol {
    
    static var name: String {
        return String(describing: self)
    }
    
}

