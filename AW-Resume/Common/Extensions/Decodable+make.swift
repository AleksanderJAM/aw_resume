
import Foundation

extension Decodable {
    
    static func make(from json: [String: Any]) throws -> Self {
        let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        return try JSONDecoder().decode(Self.self, from: data)
    }
    
    static func make(form data: Data) throws -> Self {
        return try JSONDecoder().decode(Self.self, from: data)
    }
    
}
