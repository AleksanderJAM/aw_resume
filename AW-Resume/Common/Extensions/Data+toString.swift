
import Foundation

extension Date {
    
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/YYYY"
        return formatter.string(from: self)
    }
    
}
