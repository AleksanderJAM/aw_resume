
import UIKit

extension UIImageView {
    
    func setImage(from url: URL?) {
        guard let url = url else { return }
        DispatchQueue.global(qos: .background).async {
            do {
                let imageData = try Data(contentsOf: url)
                let image = UIImage(data: imageData)
                DispatchQueue.main.async {
                    self.image = image
                }
            } catch {
                DispatchQueue.main.async {
                    self.image = nil
                }
                print(error)
            }
        }
    }
    
}
