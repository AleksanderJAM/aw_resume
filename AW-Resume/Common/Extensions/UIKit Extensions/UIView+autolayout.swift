
import UIKit

extension UIView {
    
    @discardableResult
    func manualLayoutable() -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        return self
    }
    
    /// Constrains a given view to all 4 sides
    /// of its containing view with a constant of given value. Default value is 0.
    ///
    /// - Parameter view: view to constrain based on self autolayout system
    func edgeConstrain(view: UIView,
                                   constant: CGFloat = 0.0) {
        edgeConstrain(view: view, top: constant, left: constant, right: -constant, bottom: -constant)
    }
    
    /// Constrains a given view to all 4 sides
    /// of its containing view with a constant of given value. Default value is 0.
    ///
    /// - Parameter view: view to constrain based on self autolayout system
    func edgeConstrain(view: UIView,
                       top: CGFloat? = 0.0,
                       left: CGFloat? = 0.0,
                       right: CGFloat? = 0.0,
                       bottom: CGFloat? = 0.0) {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            view.topAnchor.constraint(equalTo: self.topAnchor,
                                      constant: top).isActive = true
        }
        if let left = left {
            view.leftAnchor.constraint(equalTo: self.leftAnchor,
                                       constant: left).isActive = true
        }
        if let right = right {
            view.rightAnchor.constraint(equalTo: self.rightAnchor,
                                        constant: right).isActive = true
        }
        if let bottom = bottom {
            view.bottomAnchor.constraint(equalTo: self.bottomAnchor,
                                         constant: bottom).isActive = true
        }
    }
    
}
