
import UIKit

extension UIViewController {
    
    @discardableResult
    func embedInNavigationController() -> UINavigationController {
        return AWNavigationController(rootViewController: self)
    }
    
    func showAlert(title: String?,
                   message: String,
                   confirmTitle: String = "_ok",//Strings.ok.localized,
                   confirmCompletion: @escaping () -> Void = {},
                   isCancelOptionAvailable: Bool = false,
                   cancelTitle: String = "_cancel",//Strings.cancel.localized,
                   cancelCompletion: @escaping () -> Void = {}) {
        let alertVC = UIAlertController(title: title,
                                        message: message,
                                        preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: confirmTitle,
                                        style: .default,
                                        handler: { _ in confirmCompletion() }))
        if isCancelOptionAvailable {
            alertVC.addAction(UIAlertAction(title: cancelTitle,
                                            style: .cancel,
                                            handler: { _ in cancelCompletion() }))
        }
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func show(_ error: Error,
              file: StaticString = #file,
              line: UInt = #line) {
        
        var message = error.localizedDescription
        #if DEBUG
        message += file.description + "; " + line.description
        #endif
        showAlert(title: nil, message: message)
    }

}
