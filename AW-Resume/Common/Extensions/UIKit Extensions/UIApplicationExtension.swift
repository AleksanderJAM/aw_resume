
import UIKit

extension UIApplication {
    
    var appDelegate: AppDelegate? {
        return self.delegate as? AppDelegate
    }
    
}
