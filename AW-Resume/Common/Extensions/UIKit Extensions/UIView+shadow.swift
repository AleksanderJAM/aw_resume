
import UIKit

extension UIView {
    static var defaultShadowOpacity: Float { return 0.2 }
    
    var hasShadow: Bool {
        return self.layer.shadowColor == UIColor.black.cgColor && self.layer.shadowRadius > 0
    }
    
    func addShadow(to side: ViewSide = .all, withOpacity opaticy: Float = 0.2) {
        var offset: CGSize {
            switch side {
            case .top:
                return CGSize(width: 1.0, height: -2.0)
            case .bottom:
                return CGSize(width: 0.0, height: 2.0)
            case .left:
                return CGSize(width: -2.0, height: 0.0)
            case .right:
                return CGSize(width: 2.0, height: 0.0)
            case .topAndBottom, .leftAndRight, .all:
                return CGSize.zero
            }
        }
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 10
        layer.shadowOffset = offset
        layer.shadowOpacity = opaticy
    }
    
    enum ViewSide {
        case left, right, top, bottom, topAndBottom, leftAndRight, all
    }
    
}
