
import UIKit

extension UIView {
    
    func roundCorners(with radius: CGFloat = 5.0) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
}
