
import UIKit

class AWViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupBackItem()
    }
    
    private func setupBackItem() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}
