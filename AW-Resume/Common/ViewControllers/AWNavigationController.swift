
import UIKit

class AWNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDesign()
    }
    
    private func setupDesign() {
        self.navigationBar.isTranslucent = true
        self.navigationBar.isOpaque = false
        self.navigationBar.tintColor = .appDarkText
        self.navigationBar.barTintColor = .clear
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
    }
}
