
import UIKit
import SafariServices

class DetailsViewController<ViewModel: BaseViewModel, Cell: DetailsCell>: AWViewController, UITableViewDelegate, UITableViewDataSource {
    var viewModel: ViewModel
    
    private(set) var tableView: UITableView!
    
    // MARK: - Lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.viewModel = ViewModel()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.buildView()
        self.setupPresentationLogic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.manageNavigationBar()
    }
    
    // MARK: - Private setup methods
    
    private func buildView() {
        let builder = DetailsViewBuilder(controller: self)
        self.view = builder.buildView()
        self.tableView = builder.buildTableView()
        self.tableView.refreshControl = builder.buildRefreshControl()
        builder.setupViews()
    }
    
    private func setupPresentationLogic() {
        self.setupTableView()
    }
    
    private func setupTableView() {
        self.tableView.register(Cell.self,
                                forCellReuseIdentifier: Cell.name)
        self.tableView.refreshControl?.addTarget(self, action: #selector(getData), for: .valueChanged)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func manageNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - Helper methods
    
    func show(_ website: URL) {
        let safariViewController = SFSafariViewController(url: website)
        self.present(safariViewController, animated: true, completion: nil)
    }
    
    // MARK: - Get data
    
    @objc private func getData() {
        self.viewModel.getData(success: { [weak self] in self?.handleRequestSuccess() },
                               failure: { [weak self] in self?.handleRequestFailure($0) })
    }
    
    private func handleRequestSuccess() {
        self.tableView.refreshControl?.endRefreshing()
        self.tableView.reloadData()
    }
    
    private func handleRequestFailure(_ error: Error) {
        self.tableView.refreshControl?.endRefreshing()
        self.show(error)
    }
    
    // MARK: - TableView delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.name,
                                                 for: indexPath)
        
        if let cell = cell as? Cell,
            let accordingEducationItem = viewModel.items[safe: indexPath.row] {
            cell.configure(using: accordingEducationItem)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
    
}
