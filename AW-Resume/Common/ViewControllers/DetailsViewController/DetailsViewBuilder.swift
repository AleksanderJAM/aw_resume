
import UIKit

class DetailsViewBuilder: ViewBuilder {
    private unowned let controller: UIViewController
    private weak var tableView: UITableView!
    init(controller: UIViewController) {
        self.controller = controller
    }
    
    // MARK: - Setup methods
    
    func setupProperties() {
        self.controller.view.backgroundColor = .appBackground
    }
    
    func setupHierarchy() {
        self.controller.view.addSubview(self.tableView)
    }
    
    func setupAutoLayout() {
        self.controller.view.edgeConstrain(view: self.tableView, top: 0, left: 26, right: -26, bottom: 0)
    }
    
}

// MARK: - Build methods
extension DetailsViewBuilder {
    
    func buildTableView() -> UITableView {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.contentInset.top = 30
        tableView.contentInset.bottom = 120
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        self.tableView = tableView
        return tableView
    }
    
    func buildRefreshControl() -> UIRefreshControl {
        let control = UIRefreshControl()
        return control
    }
    
}
