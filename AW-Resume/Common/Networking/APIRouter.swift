
import Foundation

enum APIRouter {
    case user
    case experience
    case education
    case projects
    
    static var baseURLComponents: URLComponents = {
        var component = URLComponents()
        component.scheme = APIRouter.baseScheme
        component.host = APIRouter.baseHost
        return component
    }()
    
    private static let baseScheme = "https"
    private static let baseHost = "gist.githubusercontent.com"
    
    var path: String {
        switch self {
        case .user: return "/HEMIkr/9155e1e4a18d8789c8b706ac9e3b4227/raw/dc42bd6b039d2fc7bf0db9f86e91052677819ab1/awCvUser"
        case .education: return "/HEMIkr/eb18fae9a51d39b16dc5286bea3d844f/raw/19baee8cf8cb91ea36b6e8200065c5c6195ebafa/awCvEducation"
        case .experience: return "/HEMIkr/8a6699f258dde833f510dc753eccd111/raw/ba7bd72c2a5d22614bdbd1bc984589ec6772ffa1/awCvExperience"
        case .projects: return "/HEMIkr/7b85ab66c8b13c111cdf126b61c277c0/raw/3de7151602b1ea7f8701f7f42fbafd03a7cc727b/awCvProjects"
        }
    }
    
    func urlRequest() throws -> URLRequest {
        var urlComponents = APIRouter.baseURLComponents
        urlComponents.path = path
        
        guard let url = urlComponents.url else {
            throw APIRouterError.cannotCreateValidURL
        }
        
        return URLRequest(url: url)
    }

}
