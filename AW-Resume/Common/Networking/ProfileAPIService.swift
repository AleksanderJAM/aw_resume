
import Foundation

protocol ProfileAPIService: AnyObject {
    func getProfile(result: @escaping(Result<Profile, Error>) -> Void)
}

extension APIService: ProfileAPIService {
    
    func getProfile(result: @escaping(Result<Profile, Error>) -> Void) {
        do {
            let request = try APIRouter.user.urlRequest()
            self.perform(request,
                         completionHandler: { [weak self] data, response, error in
                self?.handle(response, data, error, result: result)
            })
        } catch {
            result(.failure(error))
        }
    }
    
}
