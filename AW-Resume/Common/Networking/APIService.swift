
import Foundation

class APIService {
    
    // MARK: - Properties
    
    private let successStatusCodes = (200...299)
    
    // MARK: - Private helper method
    
    private func prepareResponseData(_ response: URLResponse?,
                                     _ responseData: Data?,
                                     _ error: Error?) throws -> Data {
        if let error = error {
            throw error
        }
        
        guard
            let data = responseData,
            let response = response as? HTTPURLResponse,
            successStatusCodes.contains(response.statusCode)
            else {
                throw APIServiceError.responseError("Invalid response data")
        }
        return data
    }
    
    // MARK: - Endpoints
    
    func perform(_ request: URLRequest,
                         completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: request, completionHandler: completionHandler)
        task.resume()
    }
    
    func handle<T: Decodable>(_ response: URLResponse?,
                              _ responseData: Data?,
                              _ error: Error?,
                              result: @escaping (Result<T, Error>) -> Void) {
        do {
            let responseData = try prepareResponseData(response, responseData, error)
            let model = try T.make(form: responseData)
            result(.success(model))
        } catch {
            result(.failure(error))
        }
    }
    
}
