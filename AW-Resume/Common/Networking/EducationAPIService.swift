
import Foundation

protocol EducationAPIService: AnyObject {
    func getEducation(result: @escaping(Result<[Education], Error>) -> Void)
}

extension APIService: EducationAPIService {
    
    func getEducation(result: @escaping(Result<[Education], Error>) -> Void) {
        do {
            let request = try APIRouter.education.urlRequest()
            self.perform(request,
                         completionHandler: { [weak self] data, response, error in
                            self?.handle(response, data, error, result: result)
            })
        } catch {
            result(.failure(error))
        }
    }
    
}

