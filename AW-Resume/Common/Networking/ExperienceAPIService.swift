import Foundation

protocol ExperienceAPIService: AnyObject {
    func getExperience(result: @escaping(Result<[Experience], Error>) -> Void)
}

extension APIService: ExperienceAPIService {
    
    func getExperience(result: @escaping(Result<[Experience], Error>) -> Void) {
        do {
            let request = try APIRouter.experience.urlRequest()
            self.perform(request,
                         completionHandler: { [weak self] data, response, error in
                            self?.handle(response, data, error, result: result)
            })
        } catch {
            result(.failure(error))
        }
    }
    
}
