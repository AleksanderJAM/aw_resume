import Foundation

protocol ProjectsAPIService: AnyObject {
    func getProjects(result: @escaping(Result<[Project], Error>) -> Void)
}

extension APIService: ProjectsAPIService {
    
    func getProjects(result: @escaping(Result<[Project], Error>) -> Void) {
        do {
            let request = try APIRouter.projects.urlRequest()
            self.perform(request,
                         completionHandler: { [weak self] data, response, error in
                            self?.handle(response, data, error, result: result)
            })
        } catch {
            result(.failure(error))
        }
    }
    
}
