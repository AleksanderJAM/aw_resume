
import UIKit

extension UIColor {
    
    static let appDarkText: UIColor = .darkText
    
    /// Very light gray.
    static let appPrimary: UIColor = UIColor(red: 245/255, green: 245/255,
                                             blue: 245/255, alpha: 1)
    
    /// White. Use for container views and things like that.
    static let appSecondary: UIColor = .white
    
    /// Oragne. Default color for user intercation enabled view items.
    static let appTertiary: UIColor = .orange
    
    /// Green. Used to color minor interface interacable objects like buttons.
    static let appQuaternary: UIColor = UIColor(red: 15/255, green: 157/255,
                                                blue: 88/255, alpha: 1)
    
    static let appBackground: UIColor = .lightGray
    
}
