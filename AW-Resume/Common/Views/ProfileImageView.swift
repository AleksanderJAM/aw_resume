
import UIKit

class ProfileImageView: UIImageView {
    
    // MARK: - Lifecycle
    
    init() {
        super.init(frame: .zero)
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
        self.setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setRound()
    }
    
    // MARK: - Setup methods
    
    private func setup() {
        self.layer.borderWidth = 2
        self.tintColor = .gray
        self.layer.borderColor = UIColor.gray.cgColor
        self.backgroundColor = .white
    }
    
    // MARK: - Helper methods
    
    private func setRound() {
        let height = self.bounds.height
        self.roundCorners(with: height / 2)
    }
    
}
