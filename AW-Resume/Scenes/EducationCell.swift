
import UIKit

class EducationCell: UITableViewCell, DetailsCell {
    
    // MARK: - Properties
    
    private(set) var internalContentView: UIView!
    private(set) var mainStackView: UIStackView!
    private(set) var schoolNameLabel: UILabel!
    private(set) var schoolNameValueLabel: UILabel!
    private(set) var graduationYearLabel: UILabel!
    private(set) var graduationYearValueLabel: UILabel!
    private(set) var degreeLabel: UILabel!
    private(set) var degreeValueLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    // MARK: - Private methods
    
    private func setup() {
        let builder = EducationCellBuilder(cell: self)
        self.internalContentView = builder.buildView()
        self.mainStackView = builder.buildMainStackView()
        self.schoolNameLabel = builder.buildSchoolNameLabel()
        self.schoolNameValueLabel = builder.buildSchoolNameValueLabel()
        self.graduationYearLabel = builder.buildGraduationYearLabel()
        self.graduationYearValueLabel = builder.buildGraduationYearValueLabel()
        self.degreeLabel = builder.buildDegreeNameLabel()
        self.degreeValueLabel = builder.buildDegreeNameValueLabel()
        
        builder.setupViews()
    }
    
    // MARK: - Endpoints
    
    func configure(using model: Decodable) {
        guard let model = model as? Education else {
            assertionFailure()
            return
        }
        self.schoolNameValueLabel.text = model.schoolName
        self.graduationYearValueLabel.text = model.graduationYear
        self.degreeLabel.text = model.degreeName
    }
    
}
