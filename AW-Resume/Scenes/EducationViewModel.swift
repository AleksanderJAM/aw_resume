
import Foundation

class EducationViewModel: BaseViewModel {
    private let educationApiService: EducationAPIService = APIService()
    private(set) var items: [Education] = []
    
    required init() {}
}

extension EducationViewModel {
    
    func getData(success: @escaping () -> Void,
                 failure: @escaping (Error) -> Void) {
        educationApiService.getEducation { [weak self] result in
            guard let self = self else { return }
            do {
                self.items = try result.get()
                DispatchQueue.main.async {
                    success()
                }
            } catch {
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }
    
}
