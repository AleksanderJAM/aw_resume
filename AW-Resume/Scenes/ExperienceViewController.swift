
import UIKit

class ExperienceViewController: DetailsViewController<ExperienceViewModel, ExperienceCell>{
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = viewModel.items[safe: indexPath.row], let website = item.companyWebsiteUrl else { return }
        self.show(website)
    }
    
}
