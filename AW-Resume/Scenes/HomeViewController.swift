
import UIKit

class HomeViewController: AWViewController {
    private var viewModel: HomeLogic?
    private var router: HomeRoutingLogic?
    
    // MARK: - Properties
    
    private(set) var stackView: UIStackView!
    private(set) var profileImageView: UIImageView!
    private(set) var nameLabel: UILabel!
    private(set) var buttonsContainer: UIView!
    private(set) var educationButton: UIButton!
    private(set) var experienceButton: UIButton!
    private(set) var skillsButton: UIButton!
    private(set) var projectsButton: UIButton!
    private(set) var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Lifecycle
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        self.router = HomeRouter(controller: self)
        self.viewModel = HomeViewModel()
    }
    
    override func loadView() {
        self.buildView()
        self.setupPresentationLogic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.manageNavigationBar()
    }
    
    // MARK: - Private setup methods
    
    private func buildView() {
        let builder = HomeViewBuilder(controller: self)
        self.view = builder.buildView()
        self.stackView = builder.buildStackView()
        self.profileImageView = builder.buildProfileImageView()
        self.nameLabel = builder.buildNameLabel()
        self.buttonsContainer = builder.buildView()
        self.educationButton = builder.buildDetailsButton(for: .education)
        self.experienceButton = builder.buildDetailsButton(for: .experience)
        self.projectsButton = builder.buildDetailsButton(for: .projects)
        self.activityIndicator = builder.buildActivityIndicatorView()
        
        builder.setupViews()
    }
    
    private func setupPresentationLogic() {
        self.educationButton.addTarget(self, action: #selector(educationButtonTriggered), for: .touchUpInside)
        self.experienceButton.addTarget(self, action: #selector(experienceButtonTriggered), for: .touchUpInside)
        self.projectsButton.addTarget(self, action: #selector(projectsButtonTriggered), for: .touchUpInside)
    }
    
    private func manageNavigationBar() {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Helper methods
    
    private func setWaitingForProfileViewState(_ isWaiting: Bool) {
        self.stackView.isHidden = isWaiting
        isWaiting ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        
    }
    
    // MARK: - Get profile
    
    private func getProfile() {
        self.setWaitingForProfileViewState(true)
        self.viewModel?.getProfile(success: { [weak self] in self?.handleGetProfileSuccess() },
                                   failure: { [weak self] in self?.handleGetProfileFailure($0) })
    }
    
    private func handleGetProfileFailure(_ error: Error) {
        self.activityIndicator.stopAnimating()
        self.show(error)
    }
    
    private func handleGetProfileSuccess() {
        guard let profile = self.viewModel?.profile else {
            self.handleGetProfileFailure(HomeError.cantGetProfile)
            return
        }
        self.setWaitingForProfileViewState(false)
        self.nameLabel.text = profile.firstName + " " + profile.lastName
        self.profileImageView.setImage(from: profile.photoUrl)
    }
    
    // MARK: - Action methods
    
    @objc private func educationButtonTriggered(_ sender: UIButton) {
        self.router?.navigate(to: .education)
    }
    
    @objc private func experienceButtonTriggered(_ sender: UIButton) {
        self.router?.navigate(to: .experience)
    }
    
    @objc private func projectsButtonTriggered(_ sender: UIButton) {
        self.router?.navigate(to: .projects)
    }
    
}
