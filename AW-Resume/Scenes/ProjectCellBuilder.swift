
import UIKit

class ProjectCellBuilder: ViewBuilder {
    
    // MARK: - Properties
    
    private unowned let cell: ProjectCell
    
    // MARK: - Lifecycle
    
    init(cell: ProjectCell) {
        self.cell = cell
    }
    
    // MARK: - Setup methods
    
    func setupProperties() {
        cell.contentView.backgroundColor = .clear
        cell.internalContentView.backgroundColor = .appSecondary
        cell.internalContentView.roundCorners()
        cell.internalContentView.addShadow()
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
    }
    
    func setupHierarchy() {
        self.cell.contentView.addSubview(cell.internalContentView)
        self.cell.internalContentView.addSubview(cell.mainStackView)
        
        let roleStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        roleStackView.addArrangedSubview(cell.roleLabel)
        roleStackView.addArrangedSubview(cell.roleValueLabel)
        
        let clientStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        clientStackView.addArrangedSubview(cell.clientOriginLabel)
        clientStackView.addArrangedSubview(cell.clientOriginValueLabel)
        
        let technologiesStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        technologiesStackView.addArrangedSubview(cell.usedTechnologiesLabel)
        technologiesStackView.addArrangedSubview(cell.usedTechnologiesValueLabel)
        
        self.cell.mainStackView.addArrangedSubview(cell.projectNameLabel)
        self.cell.mainStackView.addArrangedSubview(roleStackView)
        self.cell.mainStackView.addArrangedSubview(clientStackView)
        self.cell.mainStackView.addArrangedSubview(technologiesStackView)
    }
    
    func setupAutoLayout() {
        cell.contentView.edgeConstrain(view: cell.internalContentView, constant: 12)
        cell.internalContentView.edgeConstrain(view: cell.mainStackView, constant: 12)
    }
    
}

// MARK: - Build methods
extension ProjectCellBuilder {
    
    
    func buildProjectNameLabel() -> UILabel {
        return buildLabel(type: .label)
    }
    
    func buildRoleLabel() -> UILabel {
        return buildLabel(type: .label, text: "Role:")
    }
    
    func buildRoleValueLabel() -> UILabel {
        return buildLabel()
    }
    
    func buildClientOriginLabel() -> UILabel {
        return buildLabel(type: .label, text: "Client origin:")
    }
    
    func buildClientOriginValueLabel() -> UILabel {
        return buildLabel()
    }
    
    func buildUsedTechnologiesLabel() -> UILabel {
        return buildLabel(type: .label, text: "Stack:")
    }
    
    func buildUsedTechnologiesValueLabel() -> UILabel {
        return buildLabel(numberOfLines: 0)
    }
    
}
