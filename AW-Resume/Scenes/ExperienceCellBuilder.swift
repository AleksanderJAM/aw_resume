
import UIKit

class ExperienceCellBuilder: ViewBuilder {
    
    // MARK: - Nested types
    
    enum LabelType {
        case label
        case value
    }
    
    // MARK: - Properties
    
    private unowned let cell: ExperienceCell
    
    // MARK: - Lifecycle
    
    init(cell: ExperienceCell) {
        self.cell = cell
    }
    
    // MARK: - Setup methods
    
    func setupProperties() {
        cell.contentView.backgroundColor = .clear
        cell.internalContentView.backgroundColor = .appSecondary
        cell.internalContentView.roundCorners()
        cell.internalContentView.addShadow()
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
    }
    
    func setupHierarchy() {
        self.cell.contentView.addSubview(cell.internalContentView)
        self.cell.internalContentView.addSubview(cell.mainStackView)
        
        let startDateStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        startDateStackView.addArrangedSubview(cell.startDateLabel)
        startDateStackView.addArrangedSubview(cell.startDateValueLabel)
        
        let endDateStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        endDateStackView.addArrangedSubview(cell.endDateLabel)
        endDateStackView.addArrangedSubview(cell.endDateValueLabel)
        
        let positionStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        positionStackView.addArrangedSubview(cell.positionLabel)
        positionStackView.addArrangedSubview(cell.positionValueLabel)
        
        cell.mainStackView.addArrangedSubview(cell.companyLogoImageView)
        cell.mainStackView.addArrangedSubview(cell.companyNameLabel)
        cell.mainStackView.addArrangedSubview(startDateStackView)
        cell.mainStackView.addArrangedSubview(endDateStackView)
        cell.mainStackView.addArrangedSubview(positionStackView)
    }
    
    func setupAutoLayout() {
        cell.contentView.edgeConstrain(view: cell.internalContentView, constant: 12)
        cell.internalContentView.edgeConstrain(view: cell.mainStackView, constant: 12)
        cell.companyLogoImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
}

// MARK: - Build methods
extension ExperienceCellBuilder {
    
    func buildLogoImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    
    func buildCompanyNameLabel() -> UILabel {
        return buildLabel(type: .label)
    }
    
    func buildStartDateLabel() -> UILabel {
        return buildLabel(type: .label, text: "From:")
    }
    
    func buildStartDateValueLabel() -> UILabel {
        return buildLabel()
    }
    
    func buildEndDateLabel() -> UILabel {
        return buildLabel(type: .label, text: "To:")
    }
    
    func buildEndDateValueLabel() -> UILabel {
        return buildLabel()
    }
    
    func buildPositionLabel() -> UILabel {
        return buildLabel(type: .label, text: "Position:")
    }
    
    func buildPositionValueLabel() -> UILabel {
        return buildLabel()
    }
    
}
