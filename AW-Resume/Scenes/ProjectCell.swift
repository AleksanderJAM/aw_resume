
import UIKit

class ProjectCell: UITableViewCell, DetailsCell {
    
    // MARK: - Properties
    
    private(set) var internalContentView: UIView!
    private(set) var mainStackView: UIStackView!
    private(set) var projectNameLabel: UILabel!
    private(set) var roleLabel: UILabel!
    private(set) var roleValueLabel: UILabel!
    private(set) var clientOriginLabel: UILabel!
    private(set) var clientOriginValueLabel: UILabel!
    private(set) var usedTechnologiesLabel: UILabel!
    private(set) var usedTechnologiesValueLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    // MARK: - Private methods
    
    private func setup() {
        let builder = ProjectCellBuilder(cell: self)
        self.internalContentView = builder.buildView()
        self.mainStackView = builder.buildMainStackView()
        self.projectNameLabel = builder.buildProjectNameLabel()
        self.roleLabel = builder.buildRoleLabel()
        self.roleValueLabel = builder.buildRoleValueLabel()
        self.clientOriginLabel = builder.buildClientOriginLabel()
        self.clientOriginValueLabel = builder.buildClientOriginValueLabel()
        self.usedTechnologiesLabel = builder.buildUsedTechnologiesLabel()
        self.usedTechnologiesValueLabel = builder.buildUsedTechnologiesValueLabel()
        
        builder.setupViews()
    }
    
    // MARK: - Endpoints
    
    func configure(using model: Decodable) {
        guard let model = model as? Project else {
            assertionFailure()
            return
        }
        self.projectNameLabel.text = model.name
        self.roleValueLabel.text = model.role
        self.clientOriginValueLabel.text = model.clientOrigin
        self.usedTechnologiesValueLabel.text = model.usedTechnologies.joined(separator: ", ")
        self.internalContentView.backgroundColor = model.appStoreUrl == nil ? .lightGray : .appSecondary
    }
    
}

