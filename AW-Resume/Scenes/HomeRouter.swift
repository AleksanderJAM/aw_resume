
import UIKit

protocol HomeRoutingLogic {
    func navigate(to destination: Destination)
    typealias Destination = HomeRouter.Destination
}

class HomeRouter: HomeRoutingLogic {
    private weak var viewController: HomeViewController?
    
    init(controller: HomeViewController) {
    	self.viewController = controller
    }
    
    enum Destination {
        case education
        case experience
        case projects
        
        var viewController: UIViewController {
            switch self {
            case .education: return EducationViewController()
            case .experience: return ExperienceViewController()
            case .projects: return ProjectsViewController()
            }
        }
    }
    
    func navigate(to destination: Destination) {
        self.viewController?.show(destination.viewController, sender: nil)
    }
    
}

// MARK: - Private routing logic
private extension HomeRouter {
    
    func route<T: UIViewController>(type: T.Type) {
        let destinationController = T()
        self.viewController?.show(destinationController, sender: nil)
    }
    
}
