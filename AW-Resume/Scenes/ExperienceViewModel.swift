
import Foundation

class ExperienceViewModel: BaseViewModel {
    private let experienceApiService: ExperienceAPIService = APIService()
    private(set) var items: [Experience] = []
    
    required init() {}
}

extension ExperienceViewModel {
    
    func getData(success: @escaping () -> Void,
                 failure: @escaping (Error) -> Void) {
        experienceApiService.getExperience { [weak self] result in
            guard let self = self else { return }
            do {
                self.items = try result.get()
                DispatchQueue.main.async {
                    success()
                }
            } catch {
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }
    
}
