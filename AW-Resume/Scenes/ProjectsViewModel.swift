
import Foundation

class ProjectsViewModel: BaseViewModel {
    private let projectsApiService: ProjectsAPIService = APIService()
    private(set) var items: [Project] = []
    
    required init() {}
}

extension ProjectsViewModel {
    
    func getData(success: @escaping () -> Void,
                 failure: @escaping (Error) -> Void) {
        projectsApiService.getProjects { [weak self] result in
            guard let self = self else { return }
            do {
                self.items = try result.get()
                DispatchQueue.main.async {
                    success()
                }
            } catch {
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }
    
}
