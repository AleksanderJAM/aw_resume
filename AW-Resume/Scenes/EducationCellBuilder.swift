
import UIKit

class EducationCellBuilder: ViewBuilder {
    
    // MARK: - Nested types
    
    enum LabelType {
        case label
        case value
    }
    
    // MARK: - Properties
    
    private unowned let cell: EducationCell
    
    // MARK: - Lifecycle
    
    init(cell: EducationCell) {
        self.cell = cell
    }
    
    // MARK: - Setup methods
    
    func setupProperties() {
        cell.contentView.backgroundColor = .clear
        cell.internalContentView.backgroundColor = .appSecondary
        cell.internalContentView.roundCorners()
        cell.internalContentView.addShadow()
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
    }
    
    func setupHierarchy() {
        self.cell.contentView.addSubview(cell.internalContentView)
        self.cell.internalContentView.addSubview(cell.mainStackView)
        
        let nameStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        nameStackView.addArrangedSubview(cell.schoolNameLabel)
        nameStackView.addArrangedSubview(cell.schoolNameValueLabel)
        
        let timeStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        timeStackView.addArrangedSubview(cell.graduationYearLabel)
        timeStackView.addArrangedSubview(cell.graduationYearValueLabel)
        
        let positionStackView = buildStackView(axis: .horizontal, alignment: .leading, spacing: 6)
        positionStackView.addArrangedSubview(cell.degreeLabel)
        positionStackView.addArrangedSubview(cell.degreeValueLabel)
        
        cell.mainStackView.addArrangedSubview(nameStackView)
        cell.mainStackView.addArrangedSubview(timeStackView)
        cell.mainStackView.addArrangedSubview(positionStackView)
    }
    
    func setupAutoLayout() {
        cell.contentView.edgeConstrain(view: cell.internalContentView, constant: 12)
        cell.internalContentView.edgeConstrain(view: cell.mainStackView, constant: 12)
    }
    
}

// MARK: - Build methods
extension EducationCellBuilder {
    
    func buildSchoolNameLabel() -> UILabel {
        return buildLabel(type: .label, text: "School name:")
    }
    
    func buildSchoolNameValueLabel() -> UILabel {
        return buildLabel()
    }
    
    func buildGraduationYearLabel() -> UILabel {
        return buildLabel(type: .label, text: "Graduation year:")
    }
    
    func buildGraduationYearValueLabel() -> UILabel {
        return buildLabel()
    }
    
    func buildDegreeNameLabel() -> UILabel {
        return buildLabel(type: .label, text: "Degree:")
    }
    
    func buildDegreeNameValueLabel() -> UILabel {
        return buildLabel()
    }
    
}
