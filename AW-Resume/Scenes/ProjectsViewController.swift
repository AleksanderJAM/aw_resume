
import UIKit

class ProjectsViewController: DetailsViewController<ProjectsViewModel, ProjectCell> {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = viewModel.items[safe: indexPath.row], let appsStoreURL = item.appStoreUrl else { return }
        UIApplication.shared.open(appsStoreURL, options: [:], completionHandler: nil)
    }
}
