
import UIKit

class ExperienceCell: UITableViewCell, DetailsCell {
    
    // MARK: - Properties
    
    private(set) var internalContentView: UIView!
    private(set) var mainStackView: UIStackView!
    private(set) var companyLogoImageView: UIImageView!
    private(set) var companyNameLabel: UILabel!
    private(set) var startDateLabel: UILabel!
    private(set) var startDateValueLabel: UILabel!
    private(set) var endDateLabel: UILabel!
    private(set) var endDateValueLabel: UILabel!
    private(set) var positionLabel: UILabel!
    private(set) var positionValueLabel: UILabel!
    
    // MARK: - Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    // MARK: - Private methods
    
    private func setup() {
        let builder = ExperienceCellBuilder(cell: self)
        self.internalContentView = builder.buildView()
        self.mainStackView = builder.buildMainStackView()
        self.companyLogoImageView = builder.buildLogoImageView()
        self.companyNameLabel = builder.buildCompanyNameLabel()
        self.startDateLabel = builder.buildStartDateLabel()
        self.startDateValueLabel = builder.buildStartDateValueLabel()
        self.endDateLabel = builder.buildEndDateLabel()
        self.endDateValueLabel = builder.buildEndDateValueLabel()
        self.positionLabel = builder.buildPositionLabel()
        self.positionValueLabel = builder.buildPositionValueLabel()
        
        builder.setupViews()
    }
    
    // MARK: - Endpoints
    
    func configure(using model: Decodable) {
        guard let model = model as? Experience else {
            assertionFailure()
            return
        }
        self.startDateValueLabel.text = model.startDate?.toString()
        self.endDateValueLabel.text = model.endDate?.toString()
        self.startDateLabel.superview?.isHidden = self.startDateValueLabel.text?.isEmpty ?? true
        self.endDateLabel.superview?.isHidden = self.endDateValueLabel.text?.isEmpty ?? true
        self.companyLogoImageView.setImage(from: model.companyLogoUrl)
        self.companyNameLabel.text = model.companyName
        self.positionValueLabel.text = model.position
        self.internalContentView.backgroundColor = model.companyWebsiteUrl == nil ? .appPrimary : .appSecondary
    }
    
}
