
import Foundation

enum HomeError: LocalizedError {
    case cantGetProfile
    case noProfileImage
    case invalidProfileImageData
}

protocol HomeLogic {
    func getProfile(success: @escaping () -> Void,
                    failure: @escaping (Error) -> Void)
    var profile: Profile? { get }
}

class HomeViewModel: HomeLogic {
    private let profileApiService: ProfileAPIService = APIService()
    private(set) var profile: Profile?
}

// MARK: Setup
private extension HomeViewModel {

}

// MARK: Buisness Logic methods

extension HomeViewModel {
    
    func getProfile(success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        self.profileApiService.getProfile { [weak self] result in
            guard let self = self else { return }
            do {
                self.profile = try result.get()
                DispatchQueue.main.async {
                    success()
                }
            } catch {
                DispatchQueue.main.async {
                    failure(error)
                }
            }
        }
    }
    
}
