
import UIKit

class HomeViewBuilder: ViewBuilder {
	private unowned let controller: HomeViewController
    private let buttonsHeight: CGFloat = 44
    
	init(controller: HomeViewController) {
        self.controller = controller
	}
    
    // MARK: - Setup methods

    func setupProperties() {
        controller.view.backgroundColor = .appBackground
    }
    
    func setupHierarchy() {
        let buttonsStackView = buildStackView(axis: .vertical, alignment: .fill, spacing: 30)
        
        buttonsStackView.addArrangedSubview(controller.educationButton)
        buttonsStackView.addArrangedSubview(controller.experienceButton)
        buttonsStackView.addArrangedSubview(controller.projectsButton)
        controller.buttonsContainer.addSubview(buttonsStackView)
        
        controller.view.addSubview(controller.activityIndicator)
        controller.view.addSubview(controller.stackView)
        controller.stackView.addArrangedSubview(controller.profileImageView)
        controller.stackView.addArrangedSubview(controller.nameLabel)
        controller.stackView.addArrangedSubview(controller.buttonsContainer)
    }
    
    func setupAutoLayout() {
        // Main stack view
        controller.view.edgeConstrain(view: controller.stackView,
                                      top: 105,
                                      left: 20,
                                      right: -20,
                                      bottom: -105)
        
        // Image view
        controller.profileImageView.widthAnchor
            .constraint(equalToConstant: 250).isActive = true
        controller.profileImageView.heightAnchor
            .constraint(equalTo: controller.profileImageView.widthAnchor).isActive = true

        // Buttons containers
        if let buttonsStackView = controller.buttonsContainer.subviews.first {
            buttonsStackView.centerYAnchor
                .constraint(equalTo: controller.buttonsContainer.centerYAnchor).isActive = true
            buttonsStackView.centerXAnchor
                .constraint(equalTo: controller.buttonsContainer.centerXAnchor).isActive = true
            controller.buttonsContainer.widthAnchor
                .constraint(equalTo: buttonsStackView.widthAnchor).isActive = true
        }
        
        // Buttons
        controller.educationButton.heightAnchor.constraint(equalToConstant: buttonsHeight).isActive = true
        [controller.experienceButton, controller.projectsButton].forEach {
            $0?.widthAnchor.constraint(equalTo: controller.educationButton.widthAnchor).isActive = true
            $0?.heightAnchor.constraint(equalTo: controller.educationButton.heightAnchor).isActive = true
        }
        
        // Activity Indicator
        controller.activityIndicator.centerYAnchor.constraint(equalTo: controller.view.centerYAnchor).isActive = true
        controller.activityIndicator.centerXAnchor.constraint(equalTo: controller.view.centerXAnchor).isActive = true
    }
    
}

// MARK: - Build methods
extension HomeViewBuilder {
    
    func buildNameLabel() -> UILabel {
        let label = UILabel().manualLayoutable()
        label.textColor = .appDarkText
        label.font = .appFont(ofSize: 22, weight: .semibold)
        label.textAlignment = .center
        return label
    }
    
    func buildProfileImageView() -> UIImageView {
        let imageView = ProfileImageView().manualLayoutable()
        imageView.image = UIImage.profile
        return imageView
    }
    
    func buildDetailsButton(for detailsType: ResumeDetails) -> UIButton {
        let button = UIButton()
        button.setTitle(detailsType.title, for: .normal)
        button.setTitleColor(.appPrimary, for: .normal)
        button.backgroundColor = .appQuaternary
        button.contentEdgeInsets = .init(top: 10, left: 20, bottom: 10, right: 20)
        button.roundCorners()
        return button
    }
    
    func buildActivityIndicatorView() -> UIActivityIndicatorView {
        let view = UIActivityIndicatorView(style: .whiteLarge).manualLayoutable()
        view.hidesWhenStopped = true
        return view
    }
}
