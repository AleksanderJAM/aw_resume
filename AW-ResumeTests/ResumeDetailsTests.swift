
import XCTest
@testable import AW_Resume

class ResumeDetailsTests: XCTestCase {

    func testDetailsTitle() {
        let title = ResumeDetails.education.title
        
        XCTAssert(title == "_education")
    }

}
