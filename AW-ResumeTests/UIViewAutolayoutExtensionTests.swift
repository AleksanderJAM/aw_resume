
import XCTest
@testable import AW_Resume

class UIViewAutolayoutExtensionTests: XCTestCase {

    func testManualLayoutable() {
        let view = UIView()
        view.manualLayoutable()
        
        XCTAssert(view.translatesAutoresizingMaskIntoConstraints == false)
    }
    
    func testAddEdgeConstrainedSubview() {
        let subview = UIView()
        let view = UIView()
        
        view.frame = CGRect(x: 0, y: 0, width: 240, height: 240)
        view.addEdgeConstrainedSubView(view: subview)
        view.layoutIfNeeded()
        
        XCTAssert(view.bounds == subview.frame)
    }
    
    func testAddEdgeConstrainedSubviewWithConstant() {
        let subview = UIView()
        let view = UIView()
        
        view.frame = CGRect(x: 0, y: 0, width: 240, height: 240)
        let desiredSubviewFrame = CGRect(x: 20, y: 20, width: 200, height: 200)
        view.addEdgeConstrainedSubView(view: subview, constant: 20)
        view.layoutIfNeeded()
        
        XCTAssert(subview.frame == desiredSubviewFrame)
    }

    func testEdgeConstrain() {
        let parent = UIView()
        let firstView = UIView()
        let secondView = UIView()
        let desiredSecondViewFrame = CGRect(x: 20, y: 20, width: 200, height: 200)
        
        parent.addSubview(firstView)
        parent.addSubview(secondView)
        firstView.frame = CGRect(x: 0, y: 0, width: 240, height: 240)
        firstView.edgeConstrain(view: secondView, top: 20, left: 20, right: -20, bottom: -20)
        parent.layoutIfNeeded()
        
        XCTAssert(secondView.frame == desiredSecondViewFrame)
    }
}
