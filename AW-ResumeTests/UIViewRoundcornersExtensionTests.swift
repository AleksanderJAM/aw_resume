
import XCTest
@testable import AW_Resume

class UIViewRoundcornersExtensionTests: XCTestCase {
    
    func testRoundCorners() {
        let view = UIView()
    
        view.roundCorners(with: 10)
        
        XCTAssert(view.layer.cornerRadius == 10 && view.layer.masksToBounds == true)
    }
    
}
