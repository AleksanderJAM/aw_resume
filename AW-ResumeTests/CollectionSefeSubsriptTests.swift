
import XCTest
@testable import AW_Resume

class CollectionSefeSubsriptTests: XCTestCase {

    func testSafeSubscript() {
        let array = [0, 1, 2]
        let indexOutOfRange = 3
        
        let objectOutOfRange = array[safe: indexOutOfRange]
        
        XCTAssert(objectOutOfRange == nil)
    }
    
}
