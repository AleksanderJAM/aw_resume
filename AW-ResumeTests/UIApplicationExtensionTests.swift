
import XCTest
@testable import AW_Resume

class UIApplicationExtensionTests: XCTestCase {

    func testAppDelegateShortcut() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let shortcut = UIApplication.shared.appDelegate
        
        XCTAssert(appDelegate === shortcut)
    }

}
