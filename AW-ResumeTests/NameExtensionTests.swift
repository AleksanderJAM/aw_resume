
import XCTest
@testable import AW_Resume

class NameExtensionTests: XCTestCase {

    func testName() {
        let name = AppDelegate.name
        XCTAssert(name == "AppDelegate")
    }

}
