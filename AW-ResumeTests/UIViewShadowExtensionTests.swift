
import XCTest
@testable import AW_Resume

class UIViewShadowExtensionTests: XCTestCase {

    var view: UIView!
    
    override func setUp() {
        view = UIView()
    }
    
    override func tearDown() {
        view = nil
    }
    
    func testAddShadow() {
        view.addShadow(withOpacity: 1)
        
        XCTAssert(view.layer.shadowOpacity == 1 &&
                  view.layer.masksToBounds == false)
    }
    
    func testHasShadowGetterTrue() {
        // add shadow
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 10
        view.layer.shadowOffset = .zero
        view.layer.shadowOpacity = 1
        
        // check
        XCTAssert(view.hasShadow == true)
    }
    
    func testHasShadowGetterFalse() {
        XCTAssert(view.hasShadow == false)
    }

}
