
import XCTest
@testable import AW_Resume

class APIRouterTests: XCTestCase {
    
    func testAsyncGetUser() {
        let expectation = XCTestExpectation(description: "HTTP response recieved")
        let request = try! APIRouter.user.urlRequest()
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard
                let data = data,
                let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode),
                data.isEmpty == false
                else {
                    XCTFail("HTTP request failed – user")
                    return
            }
            expectation.fulfill()
        }
        task.resume()
        wait(for: [expectation], timeout: 10)
    }
    
    func testAsyncGetEducation() {
        let expectation = XCTestExpectation(description: "HTTP response recieved")
        let request = try! APIRouter.user.urlRequest()
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard
                let data = data,
                let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode),
                data.isEmpty == false
                else {
                    XCTFail("HTTP request failed – education")
                    return
            }
            expectation.fulfill()
        }
        task.resume()
        wait(for: [expectation], timeout: 10)
    }
    
    func testAsyncGetProjects() {
        let expectation = XCTestExpectation(description: "HTTP response recieved")
        let request = try! APIRouter.user.urlRequest()
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard
                let data = data,
                let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode),
                data.isEmpty == false
                else {
                    XCTFail("HTTP request failed – projects")
                    return
            }
            expectation.fulfill()
        }
        task.resume()
        wait(for: [expectation], timeout: 10)
    }
    
    func testAsyncGetExperience() {
        let expectation = XCTestExpectation(description: "HTTP response recieved")
        let request = try! APIRouter.user.urlRequest()
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard
                let data = data,
                let response = response as? HTTPURLResponse,
                (200...299).contains(response.statusCode),
                data.isEmpty == false
                else {
                    XCTFail("HTTP request failed – experience")
                    return
            }
            expectation.fulfill()
        }
        task.resume()
        wait(for: [expectation], timeout: 10)
    }
    
}
